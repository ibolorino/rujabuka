from django.shortcuts import render, redirect, HttpResponse
from .models import Almoco, ProfileUser, Ticket, Categoria
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.messages import error



def index(request):
    if request.user.is_authenticated:
        return redirect('ver_almocos')
    return redirect('login')

@login_required
def ver_almocos(request):
    almocos = Almoco.objects.all()

    return render(request, 'tickets/almocos.html', {'almocos': almocos})

@login_required
def meus_tickets(request):
    tickets = Ticket.objects.filter(user=request.user.perfil.id, status="1")
    return render(request, 'tickets/tickets.html', {'tickets': tickets})

@login_required
def detalhe_almoco(request, id):
    almoco = Almoco.objects.get(pk=id)
    return render(request, 'tickets/detalhe_almoco.html', {'almoco': almoco})


@login_required
def comprar_ticket(request, id_almoco):
    almoco = Almoco.objects.get(pk=id_almoco)
    perfil = ProfileUser.objects.get(user=request.user)
    novo_ticket = ProfileUser.comprarTicket(self=perfil, almoco=almoco)
    if (novo_ticket != 1):
        error(request, novo_ticket)
    #almocos = Almoco.objects.all()
    #return render(request, 'tickets/almocos.html', {'almocos': almocos, 'novo_ticket': novo_ticket})
    return redirect("ver_almocos")

    
@login_required
def vender_ticket(request, id):
    perfil = ProfileUser.objects.get(user=request.user)
    perfil.venderTicket(id)
    return redirect("meus_tickets")


def get_perfil_logado(request):
    return request.user.perfil.id



#def changeSaldo(request, value, action)
#    saldoAtual = ProfileUser.getSaldo()
#    valorTicket = ProfileUser.categoria.getV
#    ProfileUser.setSaldo()


def logar(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("ver_almocos")
        else:
            HttpResponse('Usuário e/ou senha incorretos')

    return render(request, 'tickets/login.html')

def deslogar(request):
    logout(request)
    return redirect("/")

def registrar_user(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        user = User.objects.create_user(username=username, password=password, email=email)
        user.save()
        login(request, user)
        return redirect('registrar_profile') #tem que ir pra página de configurar perfil
    # ao fazer o login, caso o perfil não esteja criado, deve redirecionar
    return render(request, 'tickets/registrar_user.html')

def registrar_profile(request):
    if request.method == "POST":
        name = request.POST.get('name')
        cpf = request.POST.get('cpf')
        user = request.user
        categoria = request.POST.get('categoria')
        saldo=33
        perfil = ProfileUser.objects.create(nome=name, cpf=cpf, saldo=saldo, user=user)
        perfil.categoria.set(categoria)
        perfil.save()
        return redirect('ver_almocos')
    c = Categoria()
    categorias = Categoria.get_categorias_user(c)
    return render(request, 'tickets/registrar_profile.html', {'categorias': categorias })