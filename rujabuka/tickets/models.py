from django.db import models
from django.contrib.auth.models import User
import datetime

class Almoco(models.Model):
    data_almoco = models.DateField("Data do almoço", unique=True)
    num_tickets = models.PositiveSmallIntegerField("Número total de tickets")
    tickets_comprados = models.PositiveSmallIntegerField("Tickets Comprados", default=0)
    tickets_disponiveis = models.PositiveSmallIntegerField("Tickets disponíveis")
    cardapio = models.TextField("Cardápio", max_length=255, blank=True, null=True)

    def __str__(self):
        return "Almoço do dia " + str(self.data_almoco)

    def getTicketsComprados(self):
        return self.tickets_comprados

    def getTicketsDisponiveis(self):
        return self.tickets_disponiveis

    def getNumTickets(self):
        return self.num_tickets

    def setNumTickets(self, action):
        if (action == "compra"): #diminui tickets disponiveis e aumenta comprados
            self.tickets_disponiveis -= 1
            self.tickets_comprados += 1
        elif (action == "venda"): #aumenta tickets disponiveis e diminui comprados
            self.tickets_disponiveis += 1
            self.tickets_comprados -= 1
        self.save()
        return
    
class Ticket(models.Model):
    choices_status = [
        ('1', 'Ativo'),
        ('2', 'Usado'),
        ('3', 'Vendido'),
    ]
    data_compra = models.DateField("Data da compra")
    data_fim = models.DateField("Data da venda/uso", null=True, blank=True)
    status = models.CharField(max_length=1, choices=choices_status, default=1)
    almoco = models.ForeignKey('Almoco', models.PROTECT, 'tickets', null=False, blank=False)
    user = models.ForeignKey('ProfileUser', models.PROTECT, 'tickets_comprados', null=False, blank=False, default=1)

    def __str__(self):
        return "Ticket " + str(self.pk)


class Categoria(models.Model):
    choices_tipo = [
        ('1', 'Administração do Sistema'),
        ('2', 'Usuário do Restaurante'),
    ]
    nome = models.CharField("Nome da Categoria", max_length=50)
    tipo = models.CharField("Tipo de Categoria", max_length=1, choices=choices_tipo)
    valorTicket = models.DecimalField("Valor do ticket", max_digits=5, decimal_places=2)

    def __str__(self):
        return self.nome#self.get_tipo_display() + "/" + self.nome

    def get_categorias_user(self):
        return Categoria.objects.filter(tipo='2')

    def get_categorias_admin(self):
        return self.objects.filter(tipo='1')



class ProfileUser(models.Model):
    nome = models.CharField("Nome Completo", max_length=255)
    cpf = models.CharField("CPF (somente números)", max_length=11)
    saldo = models.DecimalField("Saldo", max_digits=5, decimal_places=2)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="perfil")
    categoria = models.ManyToManyField(Categoria, related_name="users")

    def __str__(self):
        return self.nome

    def getSaldo(self):
        return self.saldo

    def setSaldo(self, novo_saldo):
        self.saldo = novo_saldo
        return self.saldo

    def getValorTicket(self):
        return Categoria.objects.get(users=self, tipo='2').valorTicket

    def comprarTicket(self, almoco):
        data_compra = datetime.datetime.now().date()
        status = 1
        if (not self.saldoSuficiente()):
            return "Saldo Insuficiente!"
        if (not Almoco.getTicketsDisponiveis(almoco) > 0):
            return "Tickets esgotados!"
        if (self.ticketJaComprado(almoco)):
            return "Ticket já comprado para esse almoço!"
        self.atualizaSaldo("compra")
        Almoco.setNumTickets(almoco, "compra")
        novo_ticket = Ticket(data_compra=data_compra, user=self, status=status, almoco=almoco)
        novo_ticket.save()
        return 1

    def venderTicket(self, id):
        data_fim = datetime.datetime.now().date()
        status = 3
        self.atualizaSaldo("venda")
        ticket = Ticket.objects.get(pk=id)
        almoco = ticket.almoco
        Almoco.setNumTickets(almoco, "venda")
        ticket.data_fim = data_fim
        ticket.status = status
        ticket.save()
        return

    def atualizaSaldo(self, action):
        valor_ticket = self.getValorTicket()
        saldo_atual = self.getSaldo()
        saldoCompra = saldo_atual - valor_ticket
        saldoVenda = saldo_atual + valor_ticket
        novo_saldo = saldoCompra if (action == "compra") else saldoVenda
        self.setSaldo(novo_saldo)
        self.save()
        return

    def saldoSuficiente(self):
        saldo_atual = self.getSaldo()
        valor_ticket = self.getValorTicket()
        return True if saldo_atual >= valor_ticket else False

    def ticketJaComprado(self, almoco):
        return Ticket.objects.filter(user=self, almoco=almoco, status='1').exists()