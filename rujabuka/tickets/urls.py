from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name="index"),
    path('ver_almocos', views.ver_almocos, name='ver_almocos'),
    path('comprar_ticket/<int:id_almoco>/', views.comprar_ticket, name='comprar_ticket'),
    path('meus_tickets', views.meus_tickets, name='meus_tickets'),
    path('vender_ticket/<int:id>/', views.vender_ticket, name='vender_ticket'),
    path('login', views.logar, name='login'),
    path('logout', views.deslogar, name='logout'),
    path('registrar_user', views.registrar_user, name='registrar_user'),
    path('registrar_profile', views.registrar_profile, name='registrar_profile'),
    #path('searchAluno', views.searchAluno, name='searchAluno'),
    #path('viewTeste', views.viewTeste, name='viewTeste'),
    #path('ver_cursos', views.ver_cursos, name='ver_cursos'),
    path('detalhe_almoco/<int:id>/', views.detalhe_almoco, name='detalhe_almoco'),
    #path('editar_curso/<int:id>/', views.editar_curso, name='editar_curso'),
]