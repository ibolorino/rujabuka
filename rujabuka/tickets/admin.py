from django.contrib import admin
from .models import Almoco, Ticket, ProfileUser, Categoria


admin.site.register(Almoco)
admin.site.register(Ticket)
admin.site.register(ProfileUser)
admin.site.register(Categoria)